# ovu cemo klasu koristiti da bi pronasli zgodne trenutke za trgovinu bitcoinima
require 'net/http'

class Kovanice
  @@pocetna_vrijednost = 8450
  @@delta_tranzicije = 2
  @@niz_delti = []

  def self.niz_delti
    @@niz_delti
  end
  
  def osvjezi_podatke
    # ovu metodu koristimo za povlacenje i ekstrakciju podataka
    link = 'https://api.coindesk.com/v1/bpi/currentprice/EUR.json'
    uri = URI(link)
    poziv_temp = Net::HTTP.get(uri)
    poziv = JSON.parse(poziv_temp)
    trenutna_vrijednost = poziv['bpi']['EUR']['rate'].split(".").first.gsub(",","")
    puts "trenutna_vrijednost: #{trenutna_vrijednost}"
    @@niz_delti = @@niz_delti.push(trenutna_vrijednost)
    if @@niz_delti.length > 3
      @@niz_delti = @@niz_delti.slice(1, 4)
    end
    return trenutna_vrijednost
  end
  
  def usporedba
    osvjezi_podatke
    # ovu metodu koristimo da usporedimo zadnje tri promjene
    akcija = "" # ova varijabla u true stanju pokrece api call
    if @@niz_delti.length == 3
      # imamo tocno 3 zadnje promjene
      if @@niz_delti.first >= @@niz_delti.second && @@niz_delti.second >= @@niz_delti.third
        # imamo tri negativne promjene, moramo provjerit jel je delta veca od 2%
        puts "imamo tri negativne promjene"
        delta_promjene = @@niz_delti.third.to_f / @@pocetna_vrijednost * 100 - 100
        puts "delta promjene : #{delta_promjene}"
        if delta_promjene.abs > @@delta_tranzicije then 
          akcija = "prodaj" 
          puts "akcija prodaje krece"
          promjena_pocetne_vrijednosti(delta_promjene)
        else
          akcija = "miruj"
          puts "ne dovoljno spustanje"
        # situacija pada, slijedi prodaja
        end
      elsif @@niz_delti.first < @@niz_delti.second && @@niz_delti.second < @@niz_delti.third
        # imamo tri pozitivne promjene
        puts "imamo tri pozitivne promjene"
        if delta_promjene.abs > @@delta_tranzicije then 
          akcija = "kupi" 
          puts "akcija prodaje krece"
          promjena_pocetne_vrijednosti(delta_promjene)
        else
          akcija = "miruj"
          puts "ne dovoljno podizanje"
        end
      else
        puts "nema kontinuiranih promjena"
      end
    else
      # nemamo dovoljno promjena
      puts "tek je krenula skripta"
    end
    return akcija
  end

  def promjena_pocetne_vrijednosti(delta)
    puts "pokrecemo promjenu pocetne vrijednosti"
    # ovu metodu koristimo za promjenu vrijednost
    if delta > 0
      @@pocetna_vrijednost = @@pocetna_vrijednost.to_f * 1.015
    else
      @@pocetna_vrijednost = @@pocetna_vrijednost.to_f * 0.985
    end
    puts "promijenjena pocetna vrijednost: #{@@pocetna_vrijednost}"
  end

  def zovi_api
    # ovu metodu koristimo u slucaju da je metoda usporedba vratila povratnu vrijednost true
    akcija = usporedba
    unless akcija == "miruj"
      if akcija == "kupuj"
        api_akcija = "buy"
        puts "api call kupi za pocetnu vrijednost koja vrijed: #{@@pocetna_vrijednost}"
      elsif akcija == "prodaj"
        api_akcija = "sell"
        puts "api call prodaj"
      end
    end
    puts akcija
  end

# treba osvjeziti podatka svakih 5 minuta

end
testni_objekt = Kovanice.new
loop do
  testni_objekt.zovi_api
  sleep 20
  puts Kovanice.niz_delti
end